'use strict';
function Pizza(size, type) {
  const requiredArguments = 2;
  if (arguments.length !== requiredArguments) {
    throw new PizzaException(`Required two arguments, given: ${arguments.length}`)
  }

  if (!Pizza.allowedTypes.includes(type) || !Pizza.allowedSizes.includes(size)) {
    throw new PizzaException('Invalid type');
  }

  const negativeIndex = -1;
  let _size = size;
  let extrasType = [];
  let extrasPrice = [];
  this.type = type;

  Pizza.prototype.getSize = function () {
    return _size.size;
  };

  Pizza.prototype.getPrice = function () {
    return _size.price + this.type.price;
  };

  Pizza.prototype.addExtraIngredient = function (ingredient) {
    if (!ingredient) {
      throw new PizzaException('Invalid ingredient')
    }

    if (type.type === 'VEGGIE' && ingredient.extra === 'MEAT') {
      throw new PizzaException('Invalid ingredient');
    }
    
    if (extrasType.includes(ingredient.extra)) {
      throw new PizzaException('Duplicate ingredient');
    }
    
    extrasPrice.push(_size.price += ingredient.price);
    extrasType.push(ingredient.extra);
    return _size.price;
  };

  Pizza.prototype.removeExtraIngredient = function (ingredient) {
    extrasPrice.pop(this.type.price -= ingredient.price);
    const index = extrasType.indexOf(ingredient.extra);
    if (index > negativeIndex) {
      extrasType.splice(index, 1);
    }
    return this.type.price;
  };

  Pizza.prototype.getExtraIngredients = function () {
    return extrasPrice;
  };

  Pizza.prototype.getPizzaInfo = function () {
    return `Size: ${_size.size}, type: ${
      type.type
    }; extra ingredients: ${extrasType}; price: ${
      _size.price + this.type.price
    }UAH`;
  };
}

/* Sizes, types and extra ingredients */
Pizza.SIZE_S = { size: 'SMALL', price: 50 };
Pizza.SIZE_M = { size: 'MEDIUM', price: 75 };
Pizza.SIZE_L = { size: 'LARGE', price: 100 };

Pizza.TYPE_VEGGIE = { type: 'VEGGIE', price: 50 };
Pizza.TYPE_MARGHERITA = { type: 'MARGHERITA', price: 60 };
Pizza.TYPE_PEPPERONI = { type: 'PEPPERONI', price: 70 };

Pizza.EXTRA_TOMATOES = { extra: 'TOMATOES', price: 5 };
Pizza.EXTRA_CHEESE = { extra: 'CHEESE', price: 7 };
Pizza.EXTRA_MEAT = { extra: 'MEAT', price: 9 };

/* Allowed properties */
Pizza.allowedSizes = [Pizza.SIZE_S, Pizza.SIZE_M, Pizza.SIZE_L];
Pizza.allowedTypes = [Pizza.TYPE_VEGGIE, Pizza.TYPE_MARGHERITA, Pizza.TYPE_PEPPERONI];
Pizza.allowedExtraIngredients = [Pizza.EXTRA_TOMATOES, Pizza.EXTRA_CHEESE, Pizza.EXTRA_MEAT];

function PizzaException(log) {
  this.log = log;
  PizzaException.prototype.log = function () {
    return log;
  };
} 
console.log("EXAMPLE TEST CASE ");
let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_MARGHERITA);
pizza.addExtraIngredient(Pizza.EXTRA_MEAT);
console.log(`Price: ${pizza.getPrice()} UAH`);
